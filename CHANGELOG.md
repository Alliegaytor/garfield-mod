# Garfield Mod Changelog

## Update: 31 Oct @ 8:11pm
## Garfield-1

Updated for hoi 1.10
Garfieldism can no longer be boosted

## Update: 23 Nov, 2019 @ 12:27am
## Version 1.4

Updated for HoI 1.8
Fixed bugs

## Update: 12 Apr, 2018 @ 2:42am
## Version 1.3

Updated for HoI 1.5.*
Changed mod image

## Update: 6 Jul, 2017 @ 7:06pm
## Version 1.2

Division templates (again)
Made Garfield ideology orange instead of yellow
Made states owned by Garfield all cores (so you don't run dangerously low on manpower)

## Update: 5 Jul, 2017 @ 5:53pm
## Version 1.1

"Map" tag
Encoding issues
Renamed Garfield's starting division templates
Renamed Garfield's starting divisions


## Update: 4 Jul, 2017 @ 10:57pm
## Version 0.1

Thumbnail

## Update: 4 Jul, 2017 @ 10:47pm
Initial Upload

