﻿capital = 56	

oob = "GAR_1936"

add_ideas = {
	extensive_conscription
}

set_technology = {
infantry_weapons = 1
	infantry_weapons1 = 1
	tech_recon = 1
	tech_support = 1
	tech_engineers = 1
	tech_mountaineers = 1
	motorised_infantry = 1
	gw_artillery = 1
	interwar_antiair = 1
	gwtank = 1
	basic_light_tank = 1
	improved_light_tank = 1
	basic_heavy_tank = 1
	early_fighter = 1
	fighter1 = 1
	early_bomber = 1
	tactical_bomber1 = 1
	strategic_bomber1 = 1
	CAS1 = 1
	naval_bomber1 = 1

	mobile_warfare = 1
	trade_interdiction = 1
	formation_flying = 1
	synth_oil_experiments = 1
	fuel_silos = 1
	fuel_refining = 1
}

set_research_slots = 3

set_convoys = 125

1939.1.1 = {

	add_political_power = 1198

	oob = "ROB_1939"
	set_technology = {
		tech_support = 1
		tech_engineers = 1
		gw_artillery = 1
		interwar_artillery = 1
		interwar_antiair = 1
		support_weapons = 1

		#doctrines
		air_superiority = 1
		grand_battle_plan = 1
		trench_warfare = 1
		fleet_in_being = 1
		battlefleet_concentration = 1
		convoy_sailing = 1

		#electronics
		electronic_mechanical_engineering = 1
		radio = 1
		radio_detection = 1
		mechanical_computing = 1
		computing_machine = 1

		#industry
		basic_machine_tools = 1
		improved_machine_tools = 1
		advanced_machine_tools = 1
		synth_oil_experiments = 1


		construction1 = 1
		construction2 = 1
		dispersed_industry = 1
		dispersed_industry2 = 1
	}
}

set_politics = {
	ruling_party = garfield
	last_election = "1934.3.26"
	election_frequency = 60
	elections_allowed = no
}
set_popularities = {
	democratic = 22
	garfield = 76
	communism = 2
}

1939.1.1 = {
	set_politics = {
		ruling_party = garfield
		last_election = "1934.3.26"
		election_frequency = 60
		elections_allowed = no
	}
	set_popularities = {
		democratic = 22
		garfield = 76
		communism = 2
	}
}

set_convoys = 25
set_stability = 0.6
set_war_support = 0.7




create_country_leader = {
	name = "Garfield"
	desc = ""
	picture = "Garfield.tga"
	expire = "1965.1.1"
	ideology = garfield
	traits = {
		#
	}
}

